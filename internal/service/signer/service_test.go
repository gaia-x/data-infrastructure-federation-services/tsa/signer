// nolint:gosec
package signer_test

import (
	"context"
	"crypto/ecdsa"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"testing"

	"github.com/hyperledger/aries-framework-go/pkg/doc/verifiable"
	"github.com/square/go-jose/v3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/golib/errors"
	goasigner "gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/signer/gen/signer"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/signer/internal/service/signer"
	"gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/signer/internal/service/signer/signerfakes"
)

func TestService_Namespaces(t *testing.T) {
	t.Run("vault client fails to return namespaces", func(t *testing.T) {
		vault := &signerfakes.FakeVault{
			NamespacesStub: func(ctx context.Context) ([]string, error) {
				return nil, errors.New("some error")
			},
		}

		svc := signer.New(vault, []string{}, http.DefaultClient, zap.NewNop())
		namespaces, err := svc.Namespaces(context.Background())
		assert.Nil(t, namespaces)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "some error")
	})

	t.Run("vault client returns namespaces successfully", func(t *testing.T) {
		vault := &signerfakes.FakeVault{
			NamespacesStub: func(ctx context.Context) ([]string, error) {
				return []string{"transit", "hello"}, nil
			},
		}

		svc := signer.New(vault, []string{}, http.DefaultClient, zap.NewNop())
		namespaces, err := svc.Namespaces(context.Background())
		assert.NoError(t, err)
		assert.NotNil(t, namespaces)
		assert.Equal(t, namespaces, []string{"transit", "hello"})
	})
}

func TestService_NamespaceKeys(t *testing.T) {
	t.Run("error while fetching keys", func(t *testing.T) {
		vault := &signerfakes.FakeVault{
			NamespaceKeysStub: func(ctx context.Context, namespace string) ([]string, error) {
				return nil, errors.New(errors.Internal, "some error")
			},
		}
		svc := signer.New(vault, []string{}, http.DefaultClient, zap.NewNop())
		keys, err := svc.NamespaceKeys(context.Background(), &goasigner.NamespaceKeysRequest{})
		assert.Nil(t, keys)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "error getting namespace keys")
	})

	t.Run("no keys found in namespace", func(t *testing.T) {
		vault := &signerfakes.FakeVault{
			NamespaceKeysStub: func(ctx context.Context, namespace string) ([]string, error) {
				return nil, errors.New(errors.NotFound, "no keys found in namespace")
			},
		}

		svc := signer.New(vault, []string{}, http.DefaultClient, zap.NewNop())
		keys, err := svc.NamespaceKeys(context.Background(), &goasigner.NamespaceKeysRequest{})
		assert.Nil(t, keys)
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "no keys found in namespace")
	})

	t.Run("keys are retrieved successfully", func(t *testing.T) {
		vault := &signerfakes.FakeVault{
			NamespaceKeysStub: func(ctx context.Context, namespace string) ([]string, error) {
				return []string{"key1", "key3"}, nil
			},
		}

		svc := signer.New(vault, []string{}, http.DefaultClient, zap.NewNop())
		keys, err := svc.NamespaceKeys(context.Background(), &goasigner.NamespaceKeysRequest{})
		assert.NoError(t, err)
		assert.Equal(t, keys, []string{"key1", "key3"})
	})
}

func TestService_VerificationMethod(t *testing.T) {
	t.Run("signer returns error when getting key", func(t *testing.T) {
		vaultError := &signerfakes.FakeVault{
			KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
				return nil, errors.New(errors.NotFound, "key not found")
			},
		}

		svc := signer.New(vaultError, []string{}, http.DefaultClient, zap.NewNop())
		result, err := svc.VerificationMethod(context.Background(), &goasigner.VerificationMethodRequest{Key: "key1"})
		assert.Nil(t, result)
		assert.Error(t, err)
		e, ok := err.(*errors.Error)
		assert.True(t, ok)
		assert.Equal(t, errors.NotFound, e.Kind)
	})

	t.Run("signer returns ecdsa-p256 key successfully", func(t *testing.T) {
		signerOK := &signerfakes.FakeVault{
			KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
				return &signer.VaultKey{
					Name:      "key1",
					Type:      "ecdsa-p256",
					PublicKey: "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAERTx/2cyYcGVSIRP/826S32BiZxSg\nnzyXgRYmKP8N2l26ec/MwCdsHIEyraX1ZYqwMUT4wO9fqFiGsRKyMBpPnQ==\n-----END PUBLIC KEY-----\n",
				}, nil
			},
		}

		svc := signer.New(signerOK, []string{"ecdsa-p256"}, http.DefaultClient, zap.NewNop())
		result, err := svc.VerificationMethod(context.Background(), &goasigner.VerificationMethodRequest{Did: "did:web:example.com", Key: "key1"})
		assert.NotNil(t, result)
		assert.NoError(t, err)

		assert.Equal(t, "did:web:example.com#key1", result.ID)
		assert.Equal(t, "did:web:example.com", result.Controller)
		assert.Equal(t, "JsonWebKey2020", result.Type)
		assert.NotNil(t, result.PublicKeyJwk)

		pub, ok := result.PublicKeyJwk.(*jose.JSONWebKey)
		assert.True(t, ok)
		assert.NotNil(t, pub)
		assert.IsType(t, (*ecdsa.PublicKey)(nil), pub.Key)
	})
}

func TestService_VerificationMethods(t *testing.T) {
	t.Run("signer returns error when getting verification methods", func(t *testing.T) {
		vaultError := &signerfakes.FakeVault{
			KeysStub: func(ctx context.Context, namespace string) ([]*signer.VaultKey, error) {
				return nil, errors.New(errors.Internal, "some error")
			},
		}

		svc := signer.New(vaultError, []string{}, http.DefaultClient, zap.NewNop())
		result, err := svc.VerificationMethods(context.Background(), &goasigner.VerificationMethodsRequest{Namespace: "unknown"})
		assert.Nil(t, result)
		assert.Error(t, err)
		e, ok := err.(*errors.Error)
		assert.True(t, ok)
		assert.Equal(t, errors.Internal, e.Kind)
	})

	t.Run("signer return empty list if vault has no keys", func(t *testing.T) {
		vaultErrorEmptyList := &signerfakes.FakeVault{
			KeysStub: func(ctx context.Context, s string) ([]*signer.VaultKey, error) {
				return nil, errors.New(errors.NotFound, "no keys")
			},
		}
		svc := signer.New(vaultErrorEmptyList, []string{}, http.DefaultClient, zap.NewNop())
		result, err := svc.VerificationMethods(context.Background(), &goasigner.VerificationMethodsRequest{
			Did:       "did:web:example.com",
			Namespace: "nm",
		})
		assert.Nil(t, result)
		assert.NoError(t, err)
	})

	t.Run("signer returns one ecdsa-p256 key successfully", func(t *testing.T) {
		signerOK := &signerfakes.FakeVault{
			KeysStub: func(ctx context.Context, namespace string) ([]*signer.VaultKey, error) {
				return []*signer.VaultKey{
					{
						Name:      "key1",
						Type:      "ecdsa-p256",
						PublicKey: "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAERTx/2cyYcGVSIRP/826S32BiZxSg\nnzyXgRYmKP8N2l26ec/MwCdsHIEyraX1ZYqwMUT4wO9fqFiGsRKyMBpPnQ==\n-----END PUBLIC KEY-----\n",
					},
				}, nil
			},
		}

		svc := signer.New(signerOK, []string{"ecdsa-p256"}, http.DefaultClient, zap.NewNop())
		result, err := svc.VerificationMethods(context.Background(), &goasigner.VerificationMethodsRequest{
			Did:       "did:web:example.com",
			Namespace: "nm",
		})
		assert.NotNil(t, result)
		assert.NoError(t, err)

		assert.Len(t, result, 1)
		assert.Equal(t, "did:web:example.com#key1", result[0].ID)
		assert.Equal(t, "did:web:example.com", result[0].Controller)
		assert.Equal(t, "JsonWebKey2020", result[0].Type)
		assert.NotNil(t, result[0].PublicKeyJwk)

		pub, ok := result[0].PublicKeyJwk.(*jose.JSONWebKey)
		assert.True(t, ok)
		assert.NotNil(t, pub)
		assert.IsType(t, (*ecdsa.PublicKey)(nil), pub.Key)
	})

	t.Run("signer returns two ecdsa-p256 key successfully", func(t *testing.T) {
		signerOK := &signerfakes.FakeVault{
			KeysStub: func(ctx context.Context, namespace string) ([]*signer.VaultKey, error) {
				return []*signer.VaultKey{
					{
						Name:      "key1",
						Type:      "ecdsa-p256",
						PublicKey: "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAERTx/2cyYcGVSIRP/826S32BiZxSg\nnzyXgRYmKP8N2l26ec/MwCdsHIEyraX1ZYqwMUT4wO9fqFiGsRKyMBpPnQ==\n-----END PUBLIC KEY-----\n",
					},
					{
						Name:      "key2",
						Type:      "ecdsa-p256",
						PublicKey: "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAERTx/2cyYcGVSIRP/826S32BiZxSg\nnzyXgRYmKP8N2l26ec/MwCdsHIEyraX1ZYqwMUT4wO9fqFiGsRKyMBpPnQ==\n-----END PUBLIC KEY-----\n",
					},
				}, nil
			},
		}

		svc := signer.New(signerOK, []string{"ecdsa-p256"}, http.DefaultClient, zap.NewNop())
		result, err := svc.VerificationMethods(context.Background(), &goasigner.VerificationMethodsRequest{
			Did:       "did:web:example.com",
			Namespace: "nm",
		})
		assert.NotNil(t, result)
		assert.NoError(t, err)
		assert.Len(t, result, 2)

		assert.Equal(t, "did:web:example.com#key1", result[0].ID)
		assert.Equal(t, "did:web:example.com", result[0].Controller)
		assert.Equal(t, "JsonWebKey2020", result[0].Type)
		assert.NotNil(t, result[0].PublicKeyJwk)

		pub, ok := result[0].PublicKeyJwk.(*jose.JSONWebKey)
		assert.True(t, ok)
		assert.NotNil(t, pub)
		assert.IsType(t, (*ecdsa.PublicKey)(nil), pub.Key)

		assert.Equal(t, "did:web:example.com#key2", result[1].ID)
		assert.Equal(t, "JsonWebKey2020", result[1].Type)
		assert.NotNil(t, result[1].PublicKeyJwk)

		pub, ok = result[1].PublicKeyJwk.(*jose.JSONWebKey)
		assert.True(t, ok)
		assert.NotNil(t, pub)
		assert.IsType(t, (*ecdsa.PublicKey)(nil), pub.Key)
	})
}

func TestService_CredentialProof(t *testing.T) {
	tests := []struct {
		name          string
		signer        *signerfakes.FakeVault
		supportedKeys []string

		namespace  string
		keyname    string
		credential []byte

		errkind errors.Kind
		errtext string

		contexts                []string
		types                   []string
		subject                 []verifiable.Subject
		issuer                  verifiable.Issuer
		proofPurpose            string
		proofType               string
		proofVerificationMethod string
	}{
		{
			name:       "invalid credential",
			credential: []byte(invalidCredential),
			errkind:    errors.BadRequest,
			errtext:    "credential type of unknown structure",
		},
		{
			name:       "invalid credential contexts",
			credential: []byte(invalidCredentialContexts),
			errkind:    errors.BadRequest,
			errtext:    "Dereferencing a URL did not result in a valid JSON-LD context",
		},
		{
			name:       "non-existing credential contexts",
			credential: []byte(nonExistingCredentialContexts),
			errkind:    errors.BadRequest,
			errtext:    "Dereferencing a URL did not result in a valid JSON-LD context",
		},
		{
			name:       "credential with invalid subject id",
			credential: []byte(credentialWithInvalidSubjectID),
			errkind:    errors.BadRequest,
			errtext:    "invalid format of subject id",
		},
		{
			name:       "valid credential but signer cannot find key",
			namespace:  "transit",
			keyname:    "key2",
			credential: []byte(validCredential),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return nil, errors.New(errors.NotFound)
				},
			},
			errkind: errors.NotFound,
			errtext: "error getting signing key",
		},
		{
			name:       "valid credential but signer returns internal error",
			namespace:  "transit",
			keyname:    "key2",
			credential: []byte(validCredential),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return nil, errors.New(errors.Internal)
				},
			},
			errkind: errors.Internal,
			errtext: "error getting signing key",
		},
		{
			name:       "valid credential but signer returns internal error",
			namespace:  "transit",
			keyname:    "key2",
			credential: []byte(validCredential),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return nil, errors.New(errors.Internal)
				},
			},
			errkind: errors.Internal,
			errtext: "error getting signing key",
		},
		{
			name:       "valid credential but signer returns unsupported key type",
			namespace:  "transit",
			keyname:    "key2",
			credential: []byte(validCredential),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return &signer.VaultKey{
						Name: "key2",
						Type: "rsa4096",
					}, nil
				},
			},
			errkind: errors.Unknown,
			errtext: "unsupported key type",
		},
		{
			name:          "valid credential and signer key type ed25519",
			supportedKeys: []string{"ed25519"},
			namespace:     "transit",
			keyname:       "key2",
			credential:    []byte(validCredential),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return &signer.VaultKey{
						Name: "key123",
						Type: "ed25519",
					}, nil
				},
				WithKeyStub: func(namespace, key string) signer.Vault {
					return &signerfakes.FakeVault{
						SignStub: func(data []byte) ([]byte, error) {
							return []byte("test signature"), nil
						},
					}
				},
			},

			// expected attributes the VC must have
			contexts:                []string{"https://www.w3.org/2018/credentials/v1", "https://w3id.org/security/suites/jws-2020/v1", "https://schema.org"},
			subject:                 []verifiable.Subject{{ID: "", CustomFields: verifiable.CustomFields{"testdata": map[string]interface{}{"hello": "world"}}}},
			issuer:                  verifiable.Issuer{ID: "https://example.com"},
			types:                   []string{verifiable.VCType},
			proofPurpose:            "assertionMethod",
			proofType:               "JsonWebSignature2020",
			proofVerificationMethod: "https://example.com#key123",
		},
		{
			name:          "valid credential and signer key type ecdsa-p256",
			supportedKeys: []string{"ecdsa-p256"},
			namespace:     "transit",
			keyname:       "key2",
			credential:    []byte(validCredential),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return &signer.VaultKey{
						Name: "key123",
						Type: "ecdsa-p256",
					}, nil
				},
				WithKeyStub: func(namespace, key string) signer.Vault {
					return &signerfakes.FakeVault{
						SignStub: func(data []byte) ([]byte, error) {
							return base64.StdEncoding.DecodeString("aGVsbG8=")
						},
					}
				},
			},

			// expected attributes the VC must have
			contexts:                []string{"https://www.w3.org/2018/credentials/v1", "https://w3id.org/security/suites/jws-2020/v1", "https://schema.org"},
			subject:                 []verifiable.Subject{{ID: "", CustomFields: verifiable.CustomFields{"testdata": map[string]interface{}{"hello": "world"}}}},
			issuer:                  verifiable.Issuer{ID: "https://example.com"},
			types:                   []string{verifiable.VCType},
			proofPurpose:            "assertionMethod",
			proofType:               "JsonWebSignature2020",
			proofVerificationMethod: "https://example.com#key123",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			svc := signer.New(test.signer, test.supportedKeys, http.DefaultClient, zap.NewNop())

			var cred interface{}
			err := json.Unmarshal(test.credential, &cred)
			assert.NoError(t, err)

			res, err := svc.CredentialProof(context.Background(), &goasigner.CredentialProofRequest{
				Namespace:  test.namespace,
				Key:        test.keyname,
				Credential: cred,
			})
			if err != nil {
				assert.Nil(t, res)
				assert.NotEmpty(t, test.errtext)
				assert.Contains(t, err.Error(), test.errtext)
				if e, ok := err.(*errors.Error); ok {
					assert.Equal(t, test.errkind, e.Kind)
				}
			} else {
				assert.Empty(t, test.errtext)
				assert.NotNil(t, res)

				vc, ok := res.(*verifiable.Credential)
				assert.True(t, ok)

				assert.Equal(t, test.contexts, vc.Context)
				assert.Equal(t, test.subject, vc.Subject)
				assert.Equal(t, test.issuer, vc.Issuer)
				assert.Equal(t, test.types, vc.Types)
				assert.Equal(t, test.proofPurpose, vc.Proofs[0]["proofPurpose"])
				assert.Equal(t, test.proofType, vc.Proofs[0]["type"])
				assert.Equal(t, test.proofVerificationMethod, vc.Proofs[0]["verificationMethod"])
				assert.NotEmpty(t, vc.Proofs[0]["jws"])
			}
		})
	}
}

func TestService_PresentationProof(t *testing.T) {
	tests := []struct {
		name          string
		signer        *signerfakes.FakeVault
		supportedKeys []string

		issuer       string
		namespace    string
		keyname      string
		presentation []byte

		errkind errors.Kind
		errtext string

		contexts                []string
		types                   []string
		proofPurpose            string
		proofType               string
		proofVerificationMethod string
	}{
		{
			name:         "invalid verifiable presentation",
			presentation: []byte(invalidPresentation),
			errkind:      errors.BadRequest,
			errtext:      "verifiable presentation is not valid",
		},
		{
			name:         "invalid presentation contexts",
			presentation: []byte(invalidPresentationContexts),
			errkind:      errors.BadRequest,
			errtext:      "verifiable presentation is not valid",
		},
		{
			name:         "non-existing presentation contexts",
			presentation: []byte(nonExistingPresentationContexts),
			errkind:      errors.BadRequest,
			errtext:      "Dereferencing a URL did not result in a valid JSON-LD context",
		},
		{
			name:         "presentation with missing credential context",
			presentation: []byte(presentationWithMissingCredentialContext),
			errkind:      errors.BadRequest,
			errtext:      "JSON-LD doc has different structure after compaction",
		},
		{
			name:         "valid presentation but signer cannot find key",
			namespace:    "transit",
			keyname:      "key2",
			presentation: []byte(validPresentation),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return nil, errors.New(errors.NotFound)
				},
			},
			errkind: errors.NotFound,
			errtext: "error getting signing key",
		},
		{
			name:         "valid presentation but signer returns internal error",
			namespace:    "transit",
			keyname:      "key2",
			presentation: []byte(validPresentation),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return nil, errors.New(errors.Internal)
				},
			},
			errkind: errors.Internal,
			errtext: "error getting signing key",
		},
		{
			name:         "valid presentation but signer returns internal error",
			namespace:    "transit",
			keyname:      "key2",
			presentation: []byte(validPresentation),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return nil, errors.New(errors.Internal)
				},
			},
			errkind: errors.Internal,
			errtext: "error getting signing key",
		},
		{
			name:         "valid presentation but signer returns unsupported key type",
			namespace:    "transit",
			keyname:      "key2",
			presentation: []byte(validPresentation),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return &signer.VaultKey{
						Name: "key23",
						Type: "rsa4096",
					}, nil
				},
			},
			errkind: errors.Unknown,
			errtext: "unsupported key type",
		},
		{
			name:          "valid presentation and signer key type ed25519",
			supportedKeys: []string{"ed25519"},
			issuer:        "https://example.com",
			namespace:     "transit",
			keyname:       "key2",
			presentation:  []byte(validPresentation),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return &signer.VaultKey{
						Name: "key123",
						Type: "ed25519",
					}, nil
				},
				WithKeyStub: func(namespace, key string) signer.Vault {
					return &signerfakes.FakeVault{
						SignStub: func(data []byte) ([]byte, error) {
							return []byte("test signature"), nil
						},
					}
				},
			},

			// expected attributes the VC must have
			contexts:                []string{"https://www.w3.org/2018/credentials/v1", "https://www.w3.org/2018/credentials/examples/v1"},
			types:                   []string{verifiable.VPType},
			proofPurpose:            "assertionMethod",
			proofType:               "JsonWebSignature2020",
			proofVerificationMethod: "https://example.com#key123",
		},
		{
			name:          "valid presentation and signer key type ecdsa-p256",
			supportedKeys: []string{"ed25519", "ecdsa-p256"},
			issuer:        "https://example.com",
			namespace:     "transit",
			keyname:       "key2",
			presentation:  []byte(validPresentation),
			signer: &signerfakes.FakeVault{
				KeyStub: func(ctx context.Context, namespace, key string) (*signer.VaultKey, error) {
					return &signer.VaultKey{
						Name: "key123",
						Type: "ecdsa-p256",
					}, nil
				},
				WithKeyStub: func(namespace, key string) signer.Vault {
					return &signerfakes.FakeVault{
						SignStub: func(data []byte) ([]byte, error) {
							return []byte("test signature"), nil
						},
					}
				},
			},

			// expected attributes the VC must have
			contexts:                []string{"https://www.w3.org/2018/credentials/v1", "https://www.w3.org/2018/credentials/examples/v1"},
			types:                   []string{verifiable.VPType},
			proofPurpose:            "assertionMethod",
			proofType:               "JsonWebSignature2020",
			proofVerificationMethod: "https://example.com#key123",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			svc := signer.New(test.signer, test.supportedKeys, http.DefaultClient, zap.NewNop())

			var pres interface{}
			err := json.Unmarshal(test.presentation, &pres)
			assert.NoError(t, err)

			res, err := svc.PresentationProof(context.Background(), &goasigner.PresentationProofRequest{
				Issuer:       test.issuer,
				Namespace:    test.namespace,
				Key:          test.keyname,
				Presentation: pres,
			})
			if test.errtext != "" {
				require.Error(t, err)
				assert.Nil(t, res)
				assert.Contains(t, err.Error(), test.errtext)
				if e, ok := err.(*errors.Error); ok {
					assert.Equal(t, test.errkind, e.Kind)
				}
				return
			}

			assert.NotNil(t, res)
			vp, ok := res.(*verifiable.Presentation)
			assert.True(t, ok)

			assert.Equal(t, test.contexts, vp.Context)
			assert.Equal(t, test.types, vp.Type)
			assert.Equal(t, test.proofPurpose, vp.Proofs[0]["proofPurpose"])
			assert.Equal(t, test.proofType, vp.Proofs[0]["type"])
			assert.Equal(t, test.proofVerificationMethod, vp.Proofs[0]["verificationMethod"])
			assert.NotEmpty(t, vp.Proofs[0]["jws"])
		})
	}
}

// ---------- Verifiable Credentials ---------- //

//nolint:gosec
var validCredential = `{
  "@context": [
	"https://www.w3.org/2018/credentials/v1",
	"https://w3id.org/security/suites/jws-2020/v1",
	"https://schema.org"
  ],
  "credentialSubject": {
	"testdata": {"hello":"world"}
  },
  "issuanceDate": "2022-06-02T17:24:05.032533+03:00",
  "issuer": "https://example.com",
  "type": "VerifiableCredential"
}`

var credentialWithInvalidSubjectID = `{
  "@context": [
	"https://www.w3.org/2018/credentials/v1",
	"https://w3id.org/security/suites/jws-2020/v1",
	"https://schema.org"
  ],
  "credentialSubject": {
	"id":"invalid",
	"testdata": {"hello":"world"}
  },
  "issuanceDate": "2022-06-02T17:24:05.032533+03:00",
  "issuer": "https://example.com",
  "type": "VerifiableCredential"
}`

//nolint:gosec
var invalidCredential = `{"invalid":"credential"}`

//nolint:gosec
var invalidCredentialContexts = `{
	"@context": [
		"https://www.w3.org/2018/credentials/v1",
		"https://adsklfhasefugaougyasdkfhaksjdhga.com/v1"
	],
	"credentialSubject": {
		"hello": "world"
	},
	"issuanceDate": "2022-06-02T17:24:05.032533+03:00",
	"issuer": "https://example.com",
	"type": "VerifiableCredential"
}`

//nolint:gosec
var nonExistingCredentialContexts = `{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://no-schema-here.com/credentials/context"
  ],
  "credentialSubject": {
    "hello": "world"
  },
  "issuanceDate": "2022-06-02T17:24:05.032533+03:00",
  "issuer": "https://example.com",
  "type": "VerifiableCredential"
}`

// ---------- Verifiable Presentations ---------- //

var validPresentation = `{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "id": "did:123",
  "type": "VerifiablePresentation",
  "verifiableCredential": [
    {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1",
		"https://schema.org"
      ],
      "credentialSubject": {
        "allow": true,
        "taskID": "0123456789abcdef"
      },
      "issuanceDate": "2022-06-14T08:43:22.78309334Z",
      "issuer": "https://example.com",
      "type": "VerifiableCredential"
    },
    {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1",
		"https://schema.org"
      ],
      "credentialSubject": {
        "result": {
          "hello": "world"
        }
      },
      "issuanceDate": "2022-06-14T08:43:22.783102173Z",
      "issuer": "https://example.com",
      "type": "VerifiableCredential"
    }
  ]
}`

var invalidPresentation = `{"invalid":"presentation"}`

var invalidPresentationContexts = `{
  "@context": [
    "https://www.w3.org/2018/credentials/v123"
  ],
  "id": "did:123",
  "type": "VerifiablePresentation",
  "verifiableCredential": [
    {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
      ],
      "credentialSubject": {
        "allow": true,
        "taskID": "0123456789abcdef"
      },
      "issuanceDate": "2022-06-14T08:43:22.78309334Z",
      "issuer": "https://example.com",
      "type": "VerifiableCredential"
    },
    {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
      ],
      "credentialSubject": {
        "result": {
          "hello": "world"
        }
      },
      "issuanceDate": "2022-06-14T08:43:22.783102173Z",
      "issuer": "https://example.com",
      "type": "VerifiableCredential"
    }
  ]
}`

var nonExistingPresentationContexts = `{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.akdjsghadkljghadlkgjhadlkgjha.org"
  ],
  "id": "did:123",
  "type": "VerifiablePresentation",
  "verifiableCredential": [
    {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
      ],
      "credentialSubject": {
        "allow": true,
        "taskID": "0123456789abcdef"
      },
      "issuanceDate": "2022-06-14T08:43:22.78309334Z",
      "issuer": "https://example.com",
      "type": "VerifiableCredential"
    },
    {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
      ],
      "credentialSubject": {
        "result": {
          "hello": "world"
        }
      },
      "issuanceDate": "2022-06-14T08:43:22.783102173Z",
      "issuer": "https://example.com",
      "type": "VerifiableCredential"
    }
  ]
}`

var presentationWithMissingCredentialContext = `
{
  "@context": [
    "https://www.w3.org/2018/credentials/v1",
    "https://www.w3.org/2018/credentials/examples/v1"
  ],
  "id": "did:123",
  "type": "VerifiablePresentation",
  "verifiableCredential": [
    {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
      ],
      "credentialSubject": {
        "allow": true,
        "taskID": "0123456789abcdef"
      },
      "issuanceDate": "2022-06-14T08:43:22.78309334Z",
      "issuer": "https://example.com",
      "type": "VerifiableCredential"
    }
  ]
}`
