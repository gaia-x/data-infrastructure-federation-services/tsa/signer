openapi: 3.0.3
info:
    title: Signer Service
    description: Signer service exposes HTTP API for making and verifying digital signatures and proofs for Verifiable Credentials.
    version: "1.0"
servers:
    - url: http://localhost:8085
      description: Signer Server
paths:
    /liveness:
        get:
            tags:
                - health
            summary: Liveness health
            operationId: health#Liveness
            responses:
                "200":
                    description: OK response.
    /readiness:
        get:
            tags:
                - health
            summary: Readiness health
            operationId: health#Readiness
            responses:
                "200":
                    description: OK response.
    /v1/credential/proof:
        post:
            tags:
                - signer
            summary: CredentialProof signer
            description: CredentialProof adds a proof to a given Verifiable Credential.
            operationId: signer#CredentialProof
            requestBody:
                required: true
                content:
                    application/json:
                        schema:
                            $ref: '#/components/schemas/CredentialProofRequestBody'
                        example:
                            credential:
                                context:
                                    - https://www.w3.org/2018/credentials/v1
                                    - https://w3id.org/security/suites/jws-2020/v1
                                    - https://schema.org
                                type: VerifiableCredential
                                issuer: did:web:nginx:policy:policy:example:example:1.0:evaluation
                                issuancedate: 2010-01-01T19:23:24.651387237Z
                                credentialsubject:
                                    name: Alice
                                    allow: true
                            key: key1
                            namespace: transit
            responses:
                "200":
                    description: OK response.
                    content:
                        application/json:
                            schema:
                                type: string
                                example: Qui et.
                                format: binary
                            example: Et repellat.
    /v1/credential/verify:
        post:
            tags:
                - signer
            summary: VerifyCredential signer
            description: VerifyCredential verifies the proof of a Verifiable Credential.
            operationId: signer#VerifyCredential
            requestBody:
                description: Verifiable Credential in JSON format.
                required: true
                content:
                    application/json:
                        schema:
                            type: string
                            description: Verifiable Credential in JSON format.
                            example:
                                - 86
                                - 111
                                - 108
                                - 117
                                - 112
                                - 116
                                - 97
                                - 115
                                - 32
                                - 117
                                - 116
                                - 46
                            format: binary
                        example:
                            - 86
                            - 101
                            - 108
                            - 105
                            - 116
                            - 32
                            - 115
                            - 105
                            - 116
                            - 32
                            - 101
                            - 120
                            - 101
                            - 114
                            - 99
                            - 105
                            - 116
                            - 97
                            - 116
                            - 105
                            - 111
                            - 110
                            - 101
                            - 109
                            - 32
                            - 101
                            - 116
                            - 32
                            - 101
                            - 108
                            - 105
                            - 103
                            - 101
                            - 110
                            - 100
                            - 105
                            - 32
                            - 105
                            - 110
                            - 99
                            - 105
                            - 100
                            - 117
                            - 110
                            - 116
                            - 46
            responses:
                "200":
                    description: OK response.
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/VerifyResult'
                            example:
                                valid: true
    /v1/namespaces:
        get:
            tags:
                - signer
            summary: Namespaces signer
            description: Namespaces returns all keys namespaces, which corresponds to enabled Vault transit engines.
            operationId: signer#Namespaces
            responses:
                "200":
                    description: OK response.
                    content:
                        application/json:
                            schema:
                                type: array
                                items:
                                    type: string
                                    example: Ut fugit.
                                description: List of available keys namespaces.
                                example:
                                    - Vitae rem soluta quaerat odit optio.
                                    - Mollitia architecto rem beatae mollitia.
                                    - Id tempora aut.
                            example:
                                - Id vitae vel.
                                - Vel porro qui quidem unde.
                                - Quis voluptas.
                                - Qui consequatur eum nulla eaque.
    /v1/namespaces/{namespace}/keys:
        get:
            tags:
                - signer
            summary: NamespaceKeys signer
            description: NamespaceKeys returns all keys in a given namespace.
            operationId: signer#NamespaceKeys
            parameters:
                - name: namespace
                  in: path
                  description: Namespace for signing keys.
                  required: true
                  schema:
                    type: string
                    description: Namespace for signing keys.
                    example: did:web:example.com
                  example: did:web:example.com
            responses:
                "200":
                    description: OK response.
                    content:
                        application/json:
                            schema:
                                type: array
                                items:
                                    type: string
                                    example: Sed aut.
                                description: Array of key names in a given namespace.
                                example:
                                    - Quos corporis minus.
                                    - Et aut adipisci voluptatem consectetur.
                                    - Earum hic doloribus magnam.
                                    - Repudiandae quam reprehenderit sed molestias eaque.
                            example:
                                - Occaecati nam temporibus.
                                - Ex nihil.
                                - Ea qui mollitia sapiente error nostrum quae.
    /v1/presentation:
        post:
            tags:
                - signer
            summary: CreatePresentation signer
            description: CreatePresentation creates VP with proof from raw JSON data.
            operationId: signer#CreatePresentation
            requestBody:
                required: true
                content:
                    application/json:
                        schema:
                            $ref: '#/components/schemas/CreatePresentationRequestBody'
                        example:
                            context:
                                - https://w3id.org/security/suites/jws-2020/v1
                                - https://schema.org
                            data:
                                - hello: world
                                - hola: mundo
                            issuer: did:web:example.com
                            key: key1
                            namespace: transit
            responses:
                "200":
                    description: OK response.
                    content:
                        application/json:
                            schema:
                                type: string
                                example: Explicabo possimus ea.
                                format: binary
                            example: Qui quia.
    /v1/presentation/proof:
        post:
            tags:
                - signer
            summary: PresentationProof signer
            description: PresentationProof adds a proof to a given Verifiable Presentation.
            operationId: signer#PresentationProof
            requestBody:
                required: true
                content:
                    application/json:
                        schema:
                            $ref: '#/components/schemas/PresentationProofRequestBody'
                        example:
                            issuer: Qui ipsa aut non fuga iste.
                            key: key1
                            namespace: transit
                            presentation: Iure ex consequatur facilis.
            responses:
                "200":
                    description: OK response.
                    content:
                        application/json:
                            schema:
                                type: string
                                example: Tempore suscipit ut occaecati.
                                format: binary
                            example: Sit quibusdam ipsam dolores quis.
    /v1/presentation/verify:
        post:
            tags:
                - signer
            summary: VerifyPresentation signer
            description: VerifyPresentation verifies the proof of a Verifiable Presentation.
            operationId: signer#VerifyPresentation
            requestBody:
                description: Verifiable Presentation in JSON format.
                required: true
                content:
                    application/json:
                        schema:
                            type: string
                            description: Verifiable Presentation in JSON format.
                            example:
                                - 69
                                - 111
                                - 115
                                - 32
                                - 100
                                - 111
                                - 108
                                - 111
                                - 114
                                - 101
                                - 109
                                - 32
                                - 100
                                - 111
                                - 108
                                - 111
                                - 114
                                - 101
                                - 109
                                - 113
                                - 117
                                - 101
                                - 32
                                - 113
                                - 117
                                - 105
                                - 98
                                - 117
                                - 115
                                - 100
                                - 97
                                - 109
                                - 46
                            format: binary
                        example:
                            - 77
                            - 111
                            - 108
                            - 101
                            - 115
                            - 116
                            - 105
                            - 97
                            - 101
                            - 32
                            - 101
                            - 118
                            - 101
                            - 110
                            - 105
                            - 101
                            - 116
                            - 32
                            - 118
                            - 101
                            - 114
                            - 111
                            - 32
                            - 101
                            - 115
                            - 116
                            - 32
                            - 97
                            - 108
                            - 105
                            - 113
                            - 117
                            - 97
                            - 109
                            - 46
            responses:
                "200":
                    description: OK response.
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/VerifyResult'
                            example:
                                valid: false
    /v1/verification-methods/{namespace}/{did}:
        get:
            tags:
                - signer
            summary: VerificationMethods signer
            description: VerificationMethods returns all public keys in a given namespace. The result is formatted as array of DID verification methods with their controller attribute being the given DID in the request.
            operationId: signer#VerificationMethods
            parameters:
                - name: namespace
                  in: path
                  description: Keys namespace.
                  required: true
                  schema:
                    type: string
                    description: Keys namespace.
                    example: transit
                  example: transit
                - name: did
                  in: path
                  description: DID controller of the keys.
                  required: true
                  schema:
                    type: string
                    description: DID controller of the keys.
                    example: did:web:example.com
                  example: did:web:example.com
            responses:
                "200":
                    description: OK response.
                    content:
                        application/json:
                            schema:
                                type: array
                                items:
                                    $ref: '#/components/schemas/DIDVerificationMethod'
                                description: Array of public keys represented as DID Verification Methods.
                                example:
                                    - controller: did:web:example.com
                                      id: key1
                                      publicKeyJwk: Illum nesciunt.
                                      type: JsonWebKey2020
                                    - controller: did:web:example.com
                                      id: key1
                                      publicKeyJwk: Illum nesciunt.
                                      type: JsonWebKey2020
                            example:
                                - controller: did:web:example.com
                                  id: key1
                                  publicKeyJwk: Illum nesciunt.
                                  type: JsonWebKey2020
                                - controller: did:web:example.com
                                  id: key1
                                  publicKeyJwk: Illum nesciunt.
                                  type: JsonWebKey2020
                                - controller: did:web:example.com
                                  id: key1
                                  publicKeyJwk: Illum nesciunt.
                                  type: JsonWebKey2020
                                - controller: did:web:example.com
                                  id: key1
                                  publicKeyJwk: Illum nesciunt.
                                  type: JsonWebKey2020
    /v1/verification-methods/{namespace}/{key}/{did}:
        get:
            tags:
                - signer
            summary: VerificationMethod signer
            description: VerificationMethod returns a single public key formatted as DID verification method for a given namespace, key and did.
            operationId: signer#VerificationMethod
            parameters:
                - name: namespace
                  in: path
                  description: Key namespace.
                  required: true
                  schema:
                    type: string
                    description: Key namespace.
                    example: transit
                  example: transit
                - name: key
                  in: path
                  description: Name of requested key.
                  required: true
                  schema:
                    type: string
                    description: Name of requested key.
                    example: key1
                  example: key1
                - name: did
                  in: path
                  description: DID controller of the key.
                  required: true
                  schema:
                    type: string
                    description: DID controller of the key.
                    example: did:web:example.com
                  example: did:web:example.com
            responses:
                "200":
                    description: OK response.
                    content:
                        application/json:
                            schema:
                                $ref: '#/components/schemas/DIDVerificationMethod'
                            example:
                                controller: did:web:example.com
                                id: key1
                                publicKeyJwk: Iure maxime quasi exercitationem.
                                type: JsonWebKey2020
components:
    schemas:
        CreatePresentationRequestBody:
            type: object
            properties:
                context:
                    type: array
                    items:
                        type: string
                        example: Neque blanditiis nostrum nihil consequuntur est.
                    description: Additional JSONLD contexts to be specified in the VP.
                    example:
                        - https://w3id.org/security/suites/jws-2020/v1
                        - https://schema.org
                data:
                    type: array
                    items:
                        type: string
                        example: Ut dolor numquam et dolores.
                        format: binary
                    description: Raw JSON to be included inside the VP as Verifiable Credential.
                    example:
                        - hello: world
                        - hola: mundo
                issuer:
                    type: string
                    description: Issuer DID of the Verifiable Presentation.
                    example: did:web:example.com
                key:
                    type: string
                    description: Key to use for the proof signature.
                    example: key1
                namespace:
                    type: string
                    description: Key namespace.
                    example: transit
            example:
                context:
                    - https://w3id.org/security/suites/jws-2020/v1
                    - https://schema.org
                data:
                    - hello: world
                    - hola: mundo
                issuer: did:web:example.com
                key: key1
                namespace: transit
            required:
                - issuer
                - namespace
                - key
                - data
        CredentialProofRequestBody:
            type: object
            properties:
                credential:
                    type: string
                    description: Verifiable Credential in JSON format.
                    example:
                        context:
                            - https://www.w3.org/2018/credentials/v1
                            - https://w3id.org/security/suites/jws-2020/v1
                            - https://schema.org
                        type: VerifiableCredential
                        issuer: did:web:nginx:policy:policy:example:example:1.0:evaluation
                        issuancedate: 2010-01-01T19:23:24.651387237Z
                        credentialsubject:
                            name: Alice
                            allow: true
                    format: binary
                key:
                    type: string
                    description: Key to use for the proof signature (optional).
                    example: key1
                namespace:
                    type: string
                    description: Key namespace.
                    example: transit
            example:
                credential:
                    context:
                        - https://www.w3.org/2018/credentials/v1
                        - https://w3id.org/security/suites/jws-2020/v1
                        - https://schema.org
                    type: VerifiableCredential
                    issuer: did:web:nginx:policy:policy:example:example:1.0:evaluation
                    issuancedate: 2010-01-01T19:23:24.651387237Z
                    credentialsubject:
                        name: Alice
                        allow: true
                key: key1
                namespace: transit
            required:
                - namespace
                - key
                - credential
        DIDVerificationMethod:
            type: object
            properties:
                controller:
                    type: string
                    description: Controller of verification method specified as DID.
                    example: did:web:example.com
                id:
                    type: string
                    description: ID of verification method.
                    example: key1
                publicKeyJwk:
                    type: string
                    description: Public Key encoded in JWK format.
                    example: Rerum dolores ipsa.
                    format: binary
                type:
                    type: string
                    description: Type of verification method key.
                    example: JsonWebKey2020
            description: Public Key represented as DID Verification Method.
            example:
                controller: did:web:example.com
                id: key1
                publicKeyJwk: Praesentium nesciunt.
                type: JsonWebKey2020
            required:
                - id
                - type
                - controller
                - publicKeyJwk
        PresentationProofRequestBody:
            type: object
            properties:
                issuer:
                    type: string
                    description: Issuer DID used to specify proof verification info.
                    example: Omnis architecto nobis vel id.
                key:
                    type: string
                    description: Key to use for the proof signature.
                    example: key1
                namespace:
                    type: string
                    description: Key namespace.
                    example: transit
                presentation:
                    type: string
                    description: Verifiable Presentation in JSON format.
                    example: Nam ea ducimus.
                    format: binary
            example:
                issuer: Ea quas praesentium voluptas occaecati est facere.
                key: key1
                namespace: transit
                presentation: Dolores velit.
            required:
                - issuer
                - namespace
                - key
                - presentation
        VerifyResult:
            type: object
            properties:
                valid:
                    type: boolean
                    description: Valid specifies if the proof is successfully verified.
                    example: true
            example:
                valid: true
            required:
                - valid
tags:
    - name: health
      description: Health service provides health check endpoints.
    - name: signer
      description: Signer service makes digital signatures and proofs for verifiable credentials and presentations.
