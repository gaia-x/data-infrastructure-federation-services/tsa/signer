package integration

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"testing"

	"github.com/hyperledger/aries-framework-go/pkg/doc/verifiable"
	"github.com/piprate/json-gold/ld"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/signer/integration/internal/client"
)

var (
	addr   string
	loader *ld.CachingDocumentLoader
)

func initTests(t *testing.T) {
	addr = os.Getenv("SIGNER_ADDR")
	require.NotEmpty(t, addr, "environment variable SIGNER_ADDR is not set")

	if loader == nil {
		loader = ld.NewCachingDocumentLoader(ld.NewDefaultDocumentLoader(http.DefaultClient))
	}
}

func TestCreateAndVerifyCredentialProof(t *testing.T) {
	initTests(t)

	tests := []struct {
		name   string
		vc     []byte
		errMsg string
	}{
		{
			name:   "valid credential with valid id",
			vc:     []byte(credentialWithSubjectID),
			errMsg: "invalid signature",
		},
		{
			name:   "valid credential without id",
			vc:     []byte(credentialWithoutSubjectID),
			errMsg: "invalid signature",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			signer := client.NewSigner(addr)

			// create proof
			vcWithProof, err := signer.CreateCredentialProof(test.vc)
			assert.NoError(t, err)
			assert.NotNil(t, vcWithProof)

			// verify signature
			err = signer.VerifyCredentialProof(vcWithProof)
			assert.NoError(t, err)

			// parse it to object to modify credentialSubject attribute
			vc, err := verifiable.ParseCredential(
				vcWithProof,
				verifiable.WithJSONLDDocumentLoader(loader),
				verifiable.WithDisabledProofCheck(),
				verifiable.WithStrictValidation(),
				verifiable.WithJSONLDValidation(),
				verifiable.WithJSONLDOnlyValidRDF(),
			)
			assert.NoError(t, err)
			assert.NotNil(t, vc)

			subject, ok := vc.Subject.([]verifiable.Subject)
			assert.True(t, ok)

			// modify the credentialSubject by adding a new value
			subject[0].CustomFields["newKey"] = "newValue"

			// marshal the modified credential
			modifiedVC, err := json.Marshal(vc)
			assert.NoError(t, err)
			assert.NotNil(t, modifiedVC)

			err = signer.VerifyCredentialProof(modifiedVC)
			if test.errMsg != "" {
				require.Error(t, err, fmt.Sprintf("got no error but expected %q", test.errMsg))
				assert.Contains(t, err.Error(), test.errMsg)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestCreateCredentialProof(t *testing.T) {
	initTests(t)

	tests := []struct {
		name   string
		vc     []byte
		errMsg string
	}{
		{
			name: "valid credential with subject id",
			vc:   []byte(credentialWithSubjectID),
		},
		{
			name: "valid credential without subject id",
			vc:   []byte(credentialWithoutSubjectID),
		},
		{
			name:   "credential with invalid subject id",
			vc:     []byte(credentialInvalidSubjectID),
			errMsg: "invalid format of subject id",
		},
		{
			name:   "credential with numerical subject id",
			vc:     []byte(credentialWithNumericalSubjectID),
			errMsg: "verifiable credential subject of unsupported format",
		},
		{
			name:   "presentation is given instead of credential",
			vc:     []byte(presentationWithSubjectID),
			errMsg: "verifiable credential is not valid",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			signer := client.NewSigner(addr)
			vcWithProof, err := signer.CreateCredentialProof(test.vc)
			if test.errMsg != "" {
				require.Error(t, err, fmt.Sprintf("got no error but expected %q", test.errMsg))
				assert.Contains(t, err.Error(), test.errMsg)
				return
			}

			vc, err := verifiable.ParseCredential(
				vcWithProof,
				verifiable.WithJSONLDDocumentLoader(loader),
				verifiable.WithDisabledProofCheck(),
				verifiable.WithStrictValidation(),
			)
			require.NoError(t, err)
			assert.NotNil(t, vc)

			assert.NotEmpty(t, vc.Proofs)
			assert.NotEmpty(t, vc.Proofs[0]["jws"])
			assert.NotEmpty(t, vc.Proofs[0]["created"])
			assert.NotEmpty(t, vc.Proofs[0]["verificationMethod"])
			assert.Equal(t, "assertionMethod", vc.Proofs[0]["proofPurpose"])
			assert.Equal(t, "JsonWebSignature2020", vc.Proofs[0]["type"])
		})
	}
}

func TestCreateAndVerifyPresentationProof(t *testing.T) {
	initTests(t)

	tests := []struct {
		name   string
		vp     []byte
		errMsg string
	}{
		{
			name:   "presentation with valid credential subject id",
			vp:     []byte(presentationWithSubjectID),
			errMsg: "invalid signature",
		},
		{
			name:   "presentation with valid credential without subject id",
			vp:     []byte(presentationWithoutSubjectID),
			errMsg: "invalid signature",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			signer := client.NewSigner(addr)

			// create proof
			vpWithProof, err := signer.CreatePresentationProof(test.vp)
			require.NoError(t, err)
			assert.NotNil(t, vpWithProof)

			// verify signature
			err = signer.VerifyPresentationProof(vpWithProof)
			require.NoError(t, err)

			// parse it to object to modify credentialSubject attribute
			vp, err := verifiable.ParsePresentation(
				vpWithProof,
				verifiable.WithPresJSONLDDocumentLoader(loader),
				verifiable.WithPresStrictValidation(),
				verifiable.WithPresDisabledProofCheck(),
			)
			require.NoError(t, err)
			assert.NotNil(t, vp)

			for _, credential := range vp.Credentials() {
				cred, ok := credential.(map[string]interface{})
				assert.True(t, ok)

				if cred["credentialSubject"] == nil {
					continue
				}

				subject, ok := cred["credentialSubject"].(map[string]interface{})
				assert.True(t, ok)

				// modify the credentialSubject by adding a new value
				subject["newKey"] = "newValue"
			}

			// marshal the modified presentation
			modifiedVP, err := json.Marshal(vp)
			assert.NoError(t, err)
			assert.NotNil(t, modifiedVP)

			// verify the signature on the modified presentation
			err = signer.VerifyPresentationProof(modifiedVP)
			if test.errMsg != "" {
				require.Error(t, err, fmt.Sprintf("got no error but expected %q", test.errMsg))
				assert.Contains(t, err.Error(), test.errMsg)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestCreatePresentationProof(t *testing.T) {
	initTests(t)

	tests := []struct {
		name   string
		vp     []byte
		errMsg string
	}{
		{
			name: "presentation with credential subject id",
			vp:   []byte(presentationWithSubjectID),
		},
		{
			name: "presentation with credential without subject id",
			vp:   []byte(presentationWithoutSubjectID),
		},
		{
			name:   "presentation with credential with invalid subject id",
			vp:     []byte(presentationWithInvalidSubjectID),
			errMsg: "invalid format of subject id",
		},
		{
			name:   "presentation with credential with numerical subject id",
			vp:     []byte(presentationWithNumericalSubjectID),
			errMsg: "value of @id must be a string",
		},
		{
			name:   "presentation with missing credential context",
			vp:     []byte(presentationWithMissingCredentialContext),
			errMsg: "JSON-LD doc has different structure after compaction",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			signer := client.NewSigner(addr)
			vpWithProof, err := signer.CreatePresentationProof(test.vp)
			if test.errMsg != "" {
				require.Error(t, err, fmt.Sprintf("got no error but expected %q", test.errMsg))
				assert.Contains(t, err.Error(), test.errMsg)
				return
			}

			vp, err := verifiable.ParsePresentation(
				vpWithProof,
				verifiable.WithPresJSONLDDocumentLoader(loader),
				verifiable.WithPresStrictValidation(),
				verifiable.WithPresDisabledProofCheck(),
			)
			require.NoError(t, err)
			assert.NotNil(t, vp)

			assert.NotEmpty(t, vp.Proofs)
			assert.NotEmpty(t, vp.Proofs[0]["jws"])
			assert.NotEmpty(t, vp.Proofs[0]["created"])
			assert.NotEmpty(t, vp.Proofs[0]["verificationMethod"])
			assert.Equal(t, "assertionMethod", vp.Proofs[0]["proofPurpose"])
			assert.Equal(t, "JsonWebSignature2020", vp.Proofs[0]["type"])
		})
	}
}

func TestCreatePresentation(t *testing.T) {
	initTests(t)

	tests := []struct {
		name     string
		req      map[string]interface{}
		contexts []string
		errtext  string
	}{
		{
			name:    "empty request",
			errtext: "400 Bad Request",
		},
		{
			name: "invalid request",
			req: map[string]interface{}{
				"namespace": "transit",
				"key":       "key1",
				"data":      map[string]interface{}{"cred1": "value1"},
			},
			errtext: "400 Bad Request",
		},
		{
			name: "valid request with single credentialSubject entry",
			req: map[string]interface{}{
				"issuer":    "did:web:4db4-85-196-181-2.eu.ngrok.io:policy:policy:example:returnDID:1.0:evaluation",
				"namespace": "transit",
				"key":       "key1",
				"data": []map[string]interface{}{
					{"cred1": "value1"},
				},
			},
		},
		{
			name: "valid request with multiple credentialSubject entry",
			req: map[string]interface{}{
				"issuer":    "did:web:4db4-85-196-181-2.eu.ngrok.io:policy:policy:example:returnDID:1.0:evaluation",
				"namespace": "transit",
				"key":       "key1",
				"data": []map[string]interface{}{
					{"cred1": "value1"},
					{"cred2": "value2"},
				},
			},
		},
		{
			name: "valid request with additional context",
			req: map[string]interface{}{
				"issuer":    "did:web:4db4-85-196-181-2.eu.ngrok.io:policy:policy:example:returnDID:1.0:evaluation",
				"namespace": "transit",
				"key":       "key1",
				"context": []string{
					"https://schema.org",
				},
				"data": []map[string]interface{}{
					{"cred1": "value1"},
					{"cred2": "value2"},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			reqData, err := json.Marshal(test.req)
			require.NoError(t, err)

			signer := client.NewSigner(addr)
			vpWithProof, err := signer.CreatePresentation(reqData)
			if test.errtext != "" {
				require.Error(t, err, fmt.Sprintf("got no error but expected %q", test.errtext))
				assert.Contains(t, err.Error(), test.errtext)
				return
			}

			vp, err := verifiable.ParsePresentation(
				vpWithProof,
				verifiable.WithPresJSONLDDocumentLoader(loader),
				verifiable.WithPresStrictValidation(),
				verifiable.WithPresDisabledProofCheck(),
			)
			require.NoError(t, err)
			assert.NotNil(t, vp)

			assert.NotEmpty(t, vp.Proofs)
			assert.NotEmpty(t, vp.Proofs[0]["jws"])
			assert.NotEmpty(t, vp.Proofs[0]["created"])
			assert.NotEmpty(t, vp.Proofs[0]["verificationMethod"])
			assert.Equal(t, "assertionMethod", vp.Proofs[0]["proofPurpose"])
			assert.Equal(t, "JsonWebSignature2020", vp.Proofs[0]["type"])

			creds := vp.Credentials()
			requiredCreds, ok := test.req["data"].([]map[string]interface{})
			assert.True(t, ok)
			assert.Equal(t, len(creds), len(requiredCreds))

			for i, cred := range creds {
				c, ok := cred.(map[string]interface{})
				assert.True(t, ok)

				subject, ok := c["credentialSubject"].(map[string]interface{})
				assert.True(t, ok)
				assert.Equal(t, requiredCreds[i], subject)
			}
		})
	}
}
